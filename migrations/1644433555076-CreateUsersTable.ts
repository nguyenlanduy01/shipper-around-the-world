import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateUsersTable1644433555076 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'users',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'username',
            type: 'varchar',
            length: '120',
            isUnique: true,
          },
          {
            name: 'password',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'email',
            type: 'varchar',
            length: '255',
            isUnique: true,
          },
          {
            name: 'name',
            type: 'varchar',
            length: '120',
            isNullable: true,
            default: '"No name"',
          },
          {
            name: 'phone',
            type: 'varchar',
            length: '10',
          },
          {
            name: 'gender',
            type: 'boolean',
            default: true,
          },
          {
            name: 'birth',
            type: 'Date',
            isNullable: true,
          },
          {
            name: 'avatar',
            type: 'varchar',
            length: '2048',
            default:
              "'https://res.cloudinary.com/dwpqh1z7n/image/upload/v1683273938/user_avatars/c6e56503cfdd87da299f72dc416023d4_mstxim.jpg'",
          },
          {
            name: 'is_active',
            type: 'boolean',
            default: false,
          },
          {
            name: 'role',
            type: 'integer',
          },
          {
            name: 'created_at',
            type: 'DateTime',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'DateTime',
            isNullable: true,
          },
          {
            name: 'deleted_at',
            type: 'DateTime',
            isNullable: true,
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('users');
  }
}

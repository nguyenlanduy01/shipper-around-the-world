import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class CreateCommentTable1679799294677 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'comments',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'user_id',
            type: 'integer',
          },
          {
            name: 'post_id',
            type: 'integer',
          },
          {
            name: 'content',
            type: 'text',
          },
          {
            name: 'status',
            type: 'varchar',
            length: '255',
            default: "'POSTED'",
          },
          {
            name: 'created_at',
            type: 'DateTime',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'DateTime',
            isNullable: true,
          },
          {
            name: 'deleted_at',
            type: 'DateTime',
            isNullable: true,
          },
        ],
      }),
    );

    await queryRunner.createForeignKey(
      'comments',
      new TableForeignKey({
        name: 'FkComment_User',
        columnNames: ['user_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'users',
      }),
    );

    await queryRunner.createForeignKey(
      'comments',
      new TableForeignKey({
        name: 'FkComment_Post',
        columnNames: ['post_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'posts',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('comments', 'FkComment_Post');
    await queryRunner.dropForeignKey('comments', 'FkComment_User');
    await queryRunner.dropTable('comments');
  }
}

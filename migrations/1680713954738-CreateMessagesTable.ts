import {
  MigrationInterface,
  QueryRunner,
  TableForeignKey,
  Table,
} from 'typeorm';

export class CreateMessagesTable1680713954738 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'messages',
        columns: [
          {
            name: 'sender_id',
            type: 'integer',
            isPrimary: true,
          },
          {
            name: 'receiver_id',
            type: 'integer',
            isPrimary: true,
          },
          {
            name: 'content',
            type: 'text',
          },
          {
            name: 'created_at',
            type: 'DateTime',
            default: 'now()',
          },
        ],
      }),
    );

    await queryRunner.createForeignKey(
      'messages',
      new TableForeignKey({
        name: 'FkMessages_Sender',
        columnNames: ['sender_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'users',
      }),
    );

    await queryRunner.createForeignKey(
      'messages',
      new TableForeignKey({
        name: 'FkMessages_Receiver',
        columnNames: ['receiver_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'users',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('messages', 'FkMessages_Sender');
    await queryRunner.dropForeignKey('messages', 'FkMessages_Receiver');
    await queryRunner.dropTable('messages');
  }
}

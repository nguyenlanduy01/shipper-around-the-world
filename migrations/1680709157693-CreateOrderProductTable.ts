import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class CreateOrderProductTable1680709157693
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'order_product',
        columns: [
          {
            name: 'product_id',
            type: 'integer',
            isPrimary: true,
          },
          {
            name: 'order_id',
            type: 'integer',
            isPrimary: true,
          },
          {
            name: 'price',
            type: 'float',
          },
          {
            name: 'quantity',
            type: 'integer',
          },
          {
            name: 'created_at',
            type: 'DateTime',
            default: 'now()',
          },
        ],
      }),
    );

    await queryRunner.createForeignKey(
      'order_product',
      new TableForeignKey({
        name: 'FkOrderProduct_Order',
        columnNames: ['order_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'orders',
      }),
    );

    await queryRunner.createForeignKey(
      'order_product',
      new TableForeignKey({
        name: 'FkOrderProduct_Product',
        columnNames: ['product_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'products',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('order_product', 'FkOrderProduct_Order');
    await queryRunner.dropForeignKey('order_product', 'FkOrderProduct_Product');
    await queryRunner.dropTable('order_product');
  }
}

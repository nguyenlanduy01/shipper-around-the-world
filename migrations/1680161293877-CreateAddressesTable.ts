import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class CreateAddressesTable1680161293877 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'addresses',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'user_id',
            type: 'integer',
          },
          {
            name: 'city',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'district',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'commune',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'description',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'created_at',
            type: 'DateTime',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'DateTime',
            isNullable: true,
          },
          {
            name: 'deleted_at',
            type: 'DateTime',
            isNullable: true,
          },
        ],
      }),
    );

    await queryRunner.createForeignKey(
      'addresses',
      new TableForeignKey({
        name: 'FkAddresses_User',
        columnNames: ['user_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'users',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('addresses', 'FkAddresses_User');
    await queryRunner.dropTable('addresses');
  }
}

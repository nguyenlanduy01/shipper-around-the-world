import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class CreateProductTable1679628969826 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'products',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'post_id',
            type: 'integer',
          },
          {
            name: 'name',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'image',
            type: 'varchar',
            length: '2048',
          },
          {
            name: 'price',
            type: 'float',
          },
          {
            name: 'quantity',
            type: 'integer',
          },
          {
            name: 'category',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'brand',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'description',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'created_at',
            type: 'DateTime',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'DateTime',
            isNullable: true,
          },
          {
            name: 'deleted_at',
            type: 'DateTime',
            isNullable: true,
          },
        ],
      }),
    );
    await queryRunner.createForeignKey(
      'products',
      new TableForeignKey({
        name: 'FkProduct_Post',
        columnNames: ['post_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'posts',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('products', 'FkProduct_Post');
    await queryRunner.dropTable('products');
  }
}

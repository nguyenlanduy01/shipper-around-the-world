import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class CreateOrdersTable1680161293879 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'orders',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'user_id',
            type: 'integer',
          },
          {
            name: 'shipper_id',
            type: 'integer',
          },
          {
            name: 'post_id',
            type: 'integer',
          },
          {
            name: 'address_id',
            type: 'integer',
          },
          {
            name: 'ship',
            type: 'float',
          },
          {
            name: 'total_price',
            type: 'float',
          },
          {
            name: 'location',
            type: 'text',
          },
          {
            name: 'description',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'status',
            type: 'varchar',
            length: '255',
            default: "'CONFIRMING'",
          },
          {
            name: 'rate',
            type: 'int',
            isNullable: true,
          },
          {
            name: 'review',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'user_confirm',
            type: 'boolean',
            default: true,
          },
          {
            name: 'shipper_confirm',
            type: 'boolean',
            isNullable: true,
          },
          {
            name: 'reason',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'received_status',
            type: 'boolean',
            default: 'false',
          },
          {
            name: 'created_at',
            type: 'DateTime',
            default: 'now()',
          },
        ],
      }),
    );

    await queryRunner.createForeignKey(
      'orders',
      new TableForeignKey({
        name: 'FkOrders_User',
        columnNames: ['user_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'users',
      }),
    );

    await queryRunner.createForeignKey(
      'orders',
      new TableForeignKey({
        name: 'FkOrders_Shipper',
        columnNames: ['shipper_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'users',
      }),
    );

    await queryRunner.createForeignKey(
      'orders',
      new TableForeignKey({
        name: 'FkOrders_Post',
        columnNames: ['post_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'posts',
      }),
    );

    await queryRunner.createForeignKey(
      'orders',
      new TableForeignKey({
        name: 'FkOrders_Address',
        columnNames: ['address_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'addresses',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('orders', 'FkOrders_User');
    await queryRunner.dropForeignKey('orders', 'FkOrders_Shipper');
    await queryRunner.dropForeignKey('orders', 'FkOrders_Post');
    await queryRunner.dropForeignKey('orders', 'FkOrders_Address');
    await queryRunner.dropTable('orders');
  }
}

import {
  MigrationInterface,
  QueryRunner,
  TableForeignKey,
  Table,
} from 'typeorm';

export class CreateTransactionsTable1680709743184
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'transactions',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'sender_id',
            type: 'integer',
          },
          {
            name: 'receiver_id',
            type: 'integer',
          },
          {
            name: 'payment_id',
            type: 'varchar',
            isNullable: true,
          },
          {
            name: 'coin',
            type: 'float',
          },
          {
            name: 'status',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'type',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'description',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'link',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'timestamp',
            type: 'DateTime',
            default: 'now()',
          },
          {
            name: 'created_at',
            type: 'DateTime',
            default: 'now()',
          },
        ],
      }),
    );

    await queryRunner.createForeignKey(
      'transactions',
      new TableForeignKey({
        name: 'FkTransactions_Sender',
        columnNames: ['sender_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'users',
      }),
    );

    await queryRunner.createForeignKey(
      'transactions',
      new TableForeignKey({
        name: 'FkTransactions_Receiver',
        columnNames: ['receiver_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'users',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('transactions', 'FkTransactions_Sender');
    await queryRunner.dropForeignKey('transactions', 'FkTransactions_Receiver');
    await queryRunner.dropTable('transactions');
  }
}

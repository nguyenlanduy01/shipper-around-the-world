import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class CreatePostsTable1679563097835 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'posts',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'user_id',
            type: 'integer',
          },
          {
            name: 'content',
            type: 'text',
          },
          {
            name: 'status',
            type: 'varchar',
            length: '255',
            default: "'POSTED'",
          },
          {
            name: 'isHide',
            type: 'boolean',
            default: 'false',
          },
          {
            name: 'created_at',
            type: 'DateTime',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'DateTime',
            isNullable: true,
          },
          {
            name: 'deleted_at',
            type: 'DateTime',
            isNullable: true,
          },
        ],
      }),
    );

    await queryRunner.createForeignKey(
      'posts',
      new TableForeignKey({
        name: 'FkPost_User',
        columnNames: ['user_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'users',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('posts', 'FkPost_User');
    await queryRunner.dropTable('posts');
  }
}

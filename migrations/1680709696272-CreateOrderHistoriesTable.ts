import {
  MigrationInterface,
  QueryRunner,
  TableForeignKey,
  Table,
} from 'typeorm';

export class CreateOrderHistoriesTable1680709696272
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'order_histories',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'order_id',
            type: 'integer',
          },
          {
            name: 'image',
            type: 'varchar',
            length: '2048',
            isNullable: true,
          },
          {
            name: 'nation',
            type: 'varchar',
            length: '255',
            isNullable: true,
          },
          {
            name: 'city',
            type: 'varchar',
            length: '255',
            isNullable: true,
          },
          {
            name: 'district',
            type: 'varchar',
            length: '255',
            isNullable: true,
          },
          {
            name: 'commune',
            type: 'varchar',
            length: '255',
            isNullable: true,
          },
          {
            name: 'description',
            type: 'text',
            isNullable: true,
          },
          {
            name: 'status',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'created_at',
            type: 'DateTime',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'DateTime',
            isNullable: true,
          },
        ],
      }),
    );

    await queryRunner.createForeignKey(
      'order_histories',
      new TableForeignKey({
        name: 'FkOrderHistories_Order',
        columnNames: ['order_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'orders',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey(
      'order_histories',
      'FkOrderHistories_Order',
    );
    await queryRunner.dropTable('order_histories');
  }
}

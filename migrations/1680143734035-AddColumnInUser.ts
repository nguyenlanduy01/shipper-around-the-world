import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AddColumnInUser1680143734035 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumn(
      'users',
      new TableColumn({
        name: 'money',
        type: 'float',
        default: 0,
      }),
    );

    await queryRunner.addColumn(
      'users',
      new TableColumn({
        name: 'money_lock',
        type: 'float',
        default: 0,
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropColumn('users', 'money');
    await queryRunner.dropColumn('users', 'money_lock');
  }
}

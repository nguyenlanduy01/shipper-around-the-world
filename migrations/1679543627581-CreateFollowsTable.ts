import {
  MigrationInterface,
  QueryRunner,
  Table,
  TableForeignKey,
} from 'typeorm';

export class CreateFollowsTable1679543627581 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'follows',
        columns: [
          {
            name: 'follower_id',
            type: 'integer',
            isPrimary: true,
          },
          {
            name: 'following_id',
            type: 'integer',
            isPrimary: true,
          },
          {
            name: 'created_at',
            type: 'DateTime',
            default: 'now()',
          },
        ],
      }),
    );

    await queryRunner.createForeignKey(
      'follows',
      new TableForeignKey({
        name: 'FkFollower_User',
        columnNames: ['follower_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'users',
      }),
    );

    await queryRunner.createForeignKey(
      'follows',
      new TableForeignKey({
        name: 'FkFollowing_User',
        columnNames: ['following_id'],
        referencedColumnNames: ['id'],
        referencedTableName: 'users',
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropForeignKey('follows', 'FkFollower_User');
    await queryRunner.dropForeignKey('follows', 'FkFollowing_User');
    await queryRunner.dropTable('follows');
  }
}

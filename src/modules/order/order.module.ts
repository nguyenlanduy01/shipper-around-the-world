import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { jwtConstants } from 'src/auth/constants';
import { OrderRepository } from 'src/repositories/order.repository';
import { PostRepository } from 'src/repositories/post.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { OrderController } from './order.controller';
import { OrderService } from './order.service';
import { OrderProductModule } from '../order-product/order-product.module';
import { OrderHistoryModule } from '../order-history/order-history.module';
import { OrderProductRepository } from 'src/repositories/order-product.repository';
import { OrderProductService } from '../order-product/order-product.service';
import { OrderHistoryService } from '../order-history/order-history.service';
import { OrderHistoriesRepository } from 'src/repositories/order-histories.repository';
import { TransactionRepository } from 'src/repositories/transaction.repository';
import { AddressRepository } from 'src/repositories/address.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      PostRepository,
      OrderRepository,
      UserRepository,
      OrderProductRepository,
      OrderHistoriesRepository,
      TransactionRepository,
      AddressRepository,
    ]),
    JwtModule.register({
      secret: jwtConstants.secret,
    }),
    OrderProductModule,
    OrderHistoryModule,
  ],
  controllers: [OrderController],
  providers: [OrderService, OrderProductService, OrderHistoryService],
})
export class OrderModule {}

import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Render,
  Req,
} from '@nestjs/common';
import { Request } from 'express';
import { OrderService } from './order.service';
import { CreateOrderDTO } from 'src/dtos/order/create-order.dto';
import { OrderHistoryService } from '../order-history/order-history.service';
import { CreateOrderHistoryDTO } from 'src/dtos/order-history/create-order-history.dto';

@Controller('order')
export class OrderController {
  constructor(
    private readonly orderService: OrderService,
    private readonly orderHistoryService: OrderHistoryService,
  ) {}

  @Render('create-order')
  @Get('/create-order')
  async createOrderPost() {}

  @Render('order-detail')
  @Get('/detail/:id')
  async getOrderDetail(@Param('id') id: string) {
    return { id: id };
  }

  @Render('orders')
  @Get('/myorders')
  async getAllMyOrders(@Req() req: Request) {
    return this.orderService.getAllMyOrders(req);
  }

  @Get()
  async getAllOrders() {
    return this.orderService.getAllOrders();
  }

  @Get(':id')
  async getDetailOrder(@Param('id') id: string) {
    return this.orderService.getDetailOrder(id);
  }

  @Post()
  async createOrder(@Body() createOrder: CreateOrderDTO, @Req() req: Request) {
    return this.orderService.createOrder(createOrder, req);
  }

  @Patch(':id/confirm')
  async confirmOrder(@Param('id') id: string, @Req() req: Request) {
    return this.orderService.confirmOrder(id, req);
  }

  @Patch(':id/cancel')
  async cancelOrder(
    @Param('id') id: string,
    @Body('reason') reason: string,
    @Req() req: Request,
  ) {
    return this.orderService.cancelOrder(id, reason, req);
  }

  @Get(':id/orderhistories')
  async getAllOrderHistoriesById(@Param('id') id: string) {
    return this.orderHistoryService.getOrderHistoriesById(id);
  }

  @Post(':id/create')
  async createOrderHistory(
    @Param('id') id: string,
    @Body() createOrderHistoryDTO: CreateOrderHistoryDTO,
    @Req() req: Request,
  ) {
    return this.orderHistoryService.createOrderHistory(
      createOrderHistoryDTO,
      id,
      req,
    );
  }

  @Patch(':id/received')
  async receivedOrder(@Param('id') id: string, @Req() req: Request) {
    return this.orderService.receivedOrder(id, req);
  }
}

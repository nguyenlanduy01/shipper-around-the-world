import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { OrderRepository } from 'src/repositories/order.repository';
import { PostRepository } from 'src/repositories/post.repository';
import { OrderProductService } from '../order-product/order-product.service';
import { CreateOrderDTO } from 'src/dtos/order/create-order.dto';
import { Order } from 'src/entities/order.entity';
import { plainToClass } from 'class-transformer';
import { OrderStatus } from 'src/enums/order-status.enum';
import { UserRepository } from 'src/repositories/user.repository';
import { OrderHistoryService } from '../order-history/order-history.service';
import { OrderHistoriesRepository } from 'src/repositories/order-histories.repository';
import { OrderHistories } from 'src/entities/order-histories.entity';
import { OrderHistoryStatus } from 'src/enums/order-history-status.enum';
import { Transaction } from 'src/entities/transaction.entity';
import { StatusTransaction } from 'src/enums/status-transaction.enum';
import { TypeTransaction } from 'src/enums/type-transaction.enum';
import { TransactionRepository } from 'src/repositories/transaction.repository';
import { PostStatus } from 'src/enums/post-status.enum';
import { AddressRepository } from 'src/repositories/address.repository';

@Injectable()
export class OrderService {
  constructor(
    private readonly orderRepository: OrderRepository,
    private readonly jwtService: JwtService,
    private readonly orderProductService: OrderProductService,
    private readonly postRepository: PostRepository,
    private readonly userRepository: UserRepository,
    private readonly orderHistoriesRepository: OrderHistoriesRepository,
    private readonly transactionRepository: TransactionRepository,
    private readonly addressRepository: AddressRepository,
  ) {}
  async getAllOrders() {
    return await this.orderRepository.find({
      relations: ['orderProducts', 'address'],
    });
  }

  async getAllMyOrders(req: any) {
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    return await this.orderRepository.find({
      relations: ['address', 'user', 'shipper'],
      where: [{ user_id: idUser }, { shipper_id: idUser }],
      order: { created_at: 'DESC' },
    });
  }

  async getDetailOrder(id: string) {
    const idNum: number = Number(id);
    return await this.orderRepository.find({
      relations: [
        'orderProducts',
        'orderProducts.product',
        'address',
        'orderHistories',
        'shipper',
        'user',
      ],
      where: { id: idNum },
      order: { created_at: 'ASC' },
    });
  }

  async createOrder(createOrder: CreateOrderDTO, req: any) {
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );

    const user = await this.userRepository.findOne({ where: { id: idUser } });

    const orderInput = plainToClass(Order, createOrder);
    const post = await this.postRepository.findOne({
      where: { id: orderInput.post_id },
    });

    const address = await this.addressRepository.findOne({
      where: { id: orderInput.address_id },
    });

    orderInput.location = `${address.description}, ${address.commune}, ${address.district}, ${address.city}`;

    if (post.isHide === false) {
      orderInput.user_id = idUser;
      orderInput.status = OrderStatus.CONFIRMING;

      const postRelation = await this.postRepository.findOne({
        relations: ['products'],
        where: { id: orderInput.post_id },
      });

      const products = postRelation.products;

      let total_price: number = 0;

      for (const product of products) {
        total_price += Number(product.price) * Number(product.quantity);
      }

      total_price += Number(orderInput.ship);

      orderInput.total_price = total_price;

      if (Number(user.money) >= total_price) {
        const order = await this.orderRepository.save(orderInput);

        for (let product of products) {
          this.orderProductService.createOrderProduct(
            product.id,
            order.id,
            product.price,
            product.quantity,
          );
        }

        post.isHide = true;
        post.status = PostStatus.PROCESSING;
        await this.postRepository.update(post.id, post);

        user.money -= total_price;
        user.money_lock += total_price;

        await this.userRepository.update(user.id, user);

        return order;
      } else {
        throw new BadRequestException('Not enough money in the account');
      }
    } else {
      throw new BadRequestException('Order has not been created');
    }
  }

  // Shipper
  async cancelOrder(id: string, reason: string, req: any) {
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    const idNum: number = Number(id);
    const order = await this.orderRepository.findOne({ where: { id: idNum } });
    if (order) {
      const user = await this.userRepository.findOne({
        where: { id: order.user_id },
      });
      if (order.status === OrderStatus.CONFIRMING) {
        if (idUser === order.user_id && order.shipper_confirm === null) {
          order.user_confirm = false;
          order.status = OrderStatus.CANCELLED;
          order.reason = reason;

          const post = await this.postRepository.findOne({
            where: { id: order.post_id },
          });
          post.isHide = false;
          post.status = PostStatus.POSTED;
          await this.postRepository.update(order.post_id, post);

          await this.orderRepository.update(order.id, order);

          user.money_lock -= order.total_price;
          user.money += order.total_price;

          await this.userRepository.update(user.id, user);

          return order;
        } else if (
          idUser === order.shipper_id &&
          order.shipper_confirm === null &&
          order.user_confirm === true
        ) {
          order.shipper_confirm = false;
          order.status = OrderStatus.CANCELLED;
          order.reason = reason;

          const post = await this.postRepository.findOne({
            where: { id: order.post_id },
          });
          post.isHide = false;

          const user = await this.userRepository.findOne({
            where: { id: order.user_id },
          });

          await this.postRepository.update(order.post_id, post);

          await this.orderRepository.update(order.id, order);

          user.money_lock -= order.total_price;
          user.money += order.total_price;

          await this.userRepository.update(user.id, user);
          return order;
        } else {
          throw new UnauthorizedException();
        }
      } else if (order.status === OrderStatus.DONE) {
        throw new InternalServerErrorException('Order doned');
      } else if (order.status === OrderStatus.CANCELLED) {
        throw new InternalServerErrorException('Order canceled');
      } else if (order.status === OrderStatus.CONFIRMED) {
        const orderHistory = await this.orderHistoriesRepository.find({
          where: { order_id: order.id },
        });
        if (orderHistory.length <= 1) {
          if (idUser === order.user_id) {
            order.user_confirm = false;
            order.status = OrderStatus.CANCELLED;
            order.reason = reason;

            const post = await this.postRepository.findOne({
              where: { id: order.post_id },
            });
            post.isHide = false;
            post.status = PostStatus.POSTED;

            await this.postRepository.update(order.post_id, post);

            await this.orderRepository.update(order.id, order);

            user.money_lock -= order.total_price;
            user.money += order.total_price;

            await this.userRepository.update(user.id, user);

            const transactionCreate = await this.transactionRepository.findOne({
              where: { description: `Make purchase order ${order.id}` },
            });

            const transaction = new Transaction();
            transaction.coin = order.total_price;
            transaction.description = `The user canceled the order ${order.id}`;
            transaction.sender_id = order.user_id;
            transaction.receiver_id = order.shipper_id;
            transaction.status = StatusTransaction.FAIL;
            transaction.type = TypeTransaction.TRANSFER;
            transaction.timestamp = new Date();
            transaction.payment_id = transactionCreate.payment_id;
            await this.transactionRepository.save(transaction);

            return order;
          } else {
            order.shipper_confirm = false;
            order.status = OrderStatus.CANCELLED;
            order.reason = reason;

            const post = await this.postRepository.findOne({
              where: { id: order.post_id },
            });
            post.isHide = false;
            post.status = PostStatus.POSTED;

            const user = await this.userRepository.findOne({
              where: { id: order.user_id },
            });

            await this.postRepository.update(order.post_id, post);

            await this.orderRepository.update(order.id, order);

            user.money_lock -= order.total_price;
            user.money += order.total_price;

            await this.userRepository.update(user.id, user);

            const transactionCreate = await this.transactionRepository.findOne({
              where: { description: `Make purchase order ${order.id}` },
            });

            const transaction = new Transaction();
            transaction.coin = order.total_price;
            transaction.description = `The shipper canceled the order ${order.id}`;
            transaction.sender_id = order.user_id;
            transaction.receiver_id = order.shipper_id;
            transaction.status = StatusTransaction.FAIL;
            transaction.type = TypeTransaction.TRANSFER;
            transaction.timestamp = new Date();
            transaction.payment_id = transactionCreate.payment_id;
            await this.transactionRepository.save(transaction);

            return order;
          }
        } else {
          throw new BadRequestException("Can't cancel order");
        }
      }
    } else {
      throw new InternalServerErrorException('Order does not exist');
    }
  }

  //Shipper
  async confirmOrder(id: string, req: any) {
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    const idNum: number = Number(id);
    const order = await this.orderRepository.findOne({
      where: { id: idNum },
    });
    if (order) {
      if (order.shipper_id === idUser) {
        if (order.user_confirm === true && order.shipper_confirm === null) {
          order.shipper_confirm = true;
          order.status = OrderStatus.CONFIRMED;

          const post = await this.postRepository.findOne({
            where: { id: order.post_id },
          });
          post.isHide = true;
          post.status = PostStatus.PROCESSING;

          await this.postRepository.update(order.post_id, post);

          await this.orderRepository.update(order.id, order);

          const orderHistory = new OrderHistories();
          orderHistory.order_id = order.id;
          orderHistory.status = OrderHistoryStatus.PROCESSING;

          await this.orderHistoriesRepository.save(orderHistory);

          const transaction = new Transaction();
          transaction.coin = order.total_price;
          transaction.description = `Make purchase order ${order.id}`;
          transaction.sender_id = order.user_id;
          transaction.receiver_id = order.shipper_id;
          transaction.status = StatusTransaction.PROCESSING;
          transaction.type = TypeTransaction.TRANSFER;
          transaction.payment_id = Math.floor(
            Math.random() * 1000000000000,
          ).toString();
          transaction.timestamp = new Date();
          await this.transactionRepository.save(transaction);

          return order;
        } else {
          throw new BadRequestException('Order has been confirmed');
        }
      } else {
        throw new UnauthorizedException();
      }
    } else {
      throw new InternalServerErrorException('Order does not exist');
    }
  }

  async receivedOrder(id: string, req: any) {
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    const idNum: number = Number(id);
    const order = await this.orderRepository.findOne({
      where: { id: idNum },
    });
    if (order) {
      if (order.user_id === idUser) {
        const orderHistory = await this.orderHistoriesRepository.findOne({
          where: { order_id: order.id, status: OrderHistoryStatus.DONE },
        });
        if (orderHistory) {
          order.received_status = true;
          order.status = OrderStatus.DONE;
          await this.orderRepository.update(order.id, order);

          const transactionCreate = await this.transactionRepository.findOne({
            where: { description: `Make purchase order ${order.id}` },
          });

          const transaction = new Transaction();
          transaction.coin = order.total_price;
          transaction.description = `Make purchase order ${order.id}`;
          transaction.sender_id = order.user_id;
          transaction.receiver_id = order.shipper_id;
          transaction.status = StatusTransaction.DONE;
          transaction.type = TypeTransaction.TRANSFER;
          transaction.timestamp = new Date();
          transaction.payment_id = transactionCreate.payment_id;
          await this.transactionRepository.save(transaction);

          const user = await this.userRepository.findOne({
            where: { id: order.user_id },
          });
          const shipper = await this.userRepository.findOne({
            where: { id: order.shipper_id },
          });

          user.money_lock -= Number(order.total_price);
          shipper.money += Number(order.total_price);

          await this.userRepository.save([user, shipper]);

          const post = await this.postRepository.findOne({
            where: { id: order.post_id },
          });

          post.status = PostStatus.DONE;

          await this.postRepository.update(post.id, post);
        } else {
          throw new InternalServerErrorException(
            'The order has not been delivered to the address',
          );
        }
      } else {
        throw new UnauthorizedException();
      }
    } else {
      throw new InternalServerErrorException('Order does not exist');
    }
  }
}

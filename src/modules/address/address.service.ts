import {
  Injectable,
  InternalServerErrorException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { plainToClass } from 'class-transformer';
import { CreateAddressDTO } from 'src/dtos/address/create-address.dto';
import { UpdateAddressDTO } from 'src/dtos/address/update-address.dto';
import { Address } from 'src/entities/address.entity';
import { AddressRepository } from 'src/repositories/address.repository';

@Injectable()
export class AddressService {
  constructor(
    private readonly addressRepository: AddressRepository,
    private readonly jwtService: JwtService,
  ) {}

  async getAddress(id: string) {
    const idNum: number = Number(id);
    return this.addressRepository.findOne({ where: { id: idNum } });
  }

  async getAddresses(req: any) {
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    return this.addressRepository.find({ where: { user_id: idUser } });
  }

  async createAddress(createAddress: CreateAddressDTO, req: any) {
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    const address = plainToClass(Address, createAddress);
    address.user_id = idUser;
    return this.addressRepository.save(address);
  }

  async updateAddress(id: string, updateAddress: UpdateAddressDTO, req: any) {
    const idNum: number = Number(id);
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    const address = await this.addressRepository.findOne(idNum);
    if (address) {
      if (address.user_id == idUser) {
        const update = plainToClass(Address, updateAddress);
        await this.addressRepository.update(idNum, update);
        return await this.addressRepository.findOne(idNum);
      } else {
        throw new UnauthorizedException();
      }
    } else {
      throw new InternalServerErrorException('Address does not exist');
    }
  }

  async deleteAddress(id: string, req: any) {
    const idNum: number = Number(id);
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    const address = await this.addressRepository.findOne(idNum);
    if (address) {
      if (address.user_id == idUser) {
        await this.addressRepository.softDelete(idNum);
      } else {
        throw new UnauthorizedException();
      }
    } else {
      throw new InternalServerErrorException('Address does not exist');
    }
  }
}

import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { jwtConstants } from 'src/auth/constants';
import { AddressRepository } from 'src/repositories/address.repository';
import { AddressService } from './address.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([AddressRepository]),
    JwtModule.register({
      secret: jwtConstants.secret,
    }),
  ],
  providers: [AddressService],
})
export class AddressModule {}

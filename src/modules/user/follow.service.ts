import {
  HttpStatus,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { UserRepository } from 'src/repositories/user.repository';
import { FollowRepository } from 'src/repositories/follow.repository';
import { Follow } from 'src/entities/follow.entity';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class FollowService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly followRepository: FollowRepository,
    private readonly jwtService: JwtService,
  ) {}

  async getAllFollowing(req: any) {
    const idUser = this.jwtService.verify(req.cookies['access_token']).sub;
    const follow = await this.followRepository.find({
      relations: ['following'],
      where: { follower_id: idUser },
    });
    const followingUser = follow.map((value) => {
      return value.following;
    });
    return followingUser;
  }

  async getAllFollower(req: any) {
    const idUser = this.jwtService.verify(req.cookies['access_token']).sub;
    const follower = await this.followRepository.find({
      relations: ['follower'],
      where: { following_id: idUser },
    });
    const followerUser = follower.map((value) => {
      return value.follower;
    });
    return followerUser;
  }

  async getAllFollowingByID(id: string) {
    const idUser: number = Number(id);
    const follow = await this.followRepository.find({
      relations: ['following'],
      where: { follower_id: idUser },
    });
    const followingUser = follow.map((value) => {
      return value.following;
    });
    return followingUser;
  }

  async getAllFollowerByID(id: string) {
    const idUser: number = Number(id);
    const follower = await this.followRepository.find({
      relations: ['follower'],
      where: { following_id: idUser },
    });
    const followerUser = follower.map((value) => {
      return value.follower;
    });
    return followerUser;
  }

  async followUser(res: any, req: any, followingId: string) {
    const idUser = this.jwtService.verify(req.cookies['access_token']).sub;
    const followerIdNum = Number(idUser);
    const followingIdNum = Number(followingId);
    const follower = await this.userRepository.findOne({
      where: { id: followerIdNum },
    });
    const following = await this.userRepository.findOne({
      where: { id: followingIdNum },
    });
    const newFollower = new Follow();
    newFollower.follower = follower;
    newFollower.following = following;
    const follow = await this.followRepository.save(newFollower);
    res
      .status(HttpStatus.OK)
      .json({ message: 'Follow Successfully!', data: follow });
  }

  async unfollowUser(res: any, req: any, followingId: string) {
    const idUser = this.jwtService.verify(req.cookies['access_token']).sub;
    const followerIdNum = Number(idUser);
    const followingIdNum = Number(followingId);
    const id = await this.followRepository.findOne({
      where: { follower_id: followerIdNum, following_id: followingIdNum },
    });
    if (id) {
      await this.followRepository.delete(id);
      res.status(HttpStatus.OK).json({ message: 'Unfollow Successfully!' });
    } else {
      throw new InternalServerErrorException('Follow does not exist');
    }
  }
}

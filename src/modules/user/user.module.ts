import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserRepository } from 'src/repositories/user.repository';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/auth/constants';
import { FollowRepository } from 'src/repositories/follow.repository';
import { FollowService } from './follow.service';
import { PostRepository } from 'src/repositories/post.repository';
import { CommentService } from '../comment/comment.service';
import { CommentRepository } from 'src/repositories/comment.repository';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { AddressRepository } from 'src/repositories/address.repository';
import { AddressService } from '../address/address.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserRepository,
      FollowRepository,
      PostRepository,
      CommentRepository,
      AddressRepository
    ]),
    JwtModule.register({
      secret: jwtConstants.secret,
    }),
  ],
  controllers: [UserController],
  providers: [UserService, FollowService, CommentService, AddressService],
  exports: [UserService],
})
export class UserModule {}

import {
  HttpStatus,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { EditPasswordDTO } from 'src/dtos/user/edit-password.dto';
import { UpdateUserDTO } from 'src/dtos/user/update-user.dto';
import { User } from 'src/entities/user.entity';
import { UserRepository } from 'src/repositories/user.repository';
import { JwtService } from '@nestjs/jwt';
import { convertToMD5 } from 'src/utils/util';
import { CreateUserDTO } from 'src/dtos/user/create-user.dto';
import { PostRepository } from 'src/repositories/post.repository';
import { plainToClass } from 'class-transformer';
import { Not } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly postRepository: PostRepository,
    private readonly jwtService: JwtService,
  ) {}

  async getAllUser(req: any) {
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    let currentUser = await this.userRepository.findOne({
      where: { id: idUser },
    });
    if (currentUser) {
      const result = await this.userRepository.find({
        where: { id: Not(idUser) },
      });
      return result;
    } else {
      return [];
    }
  }

  async getUserById(userId: number): Promise<User> {
    const user = await this.userRepository.findOne({
      relations: [
        'followers',
        'followings',
        'posts',
        'posts.user',
        'posts.products',
        'posts.comments',
        'posts.comments.user',
      ],
      where: { id: userId },
      order: { created_at: 'DESC' },
    });
    if (!user) {
      throw new NotFoundException();
    }
    user.posts.reverse();
    return user;
  }

  async updateUser(id: number, user: UpdateUserDTO) {
    return this.userRepository.update(id, user);
  }

  async deleteUser(id: number) {
    return this.userRepository.softDelete(id);
  }

  async editPassword(editPassword: EditPasswordDTO, req: any, res: any) {
    const idUser = this.jwtService.verify(req.cookies['access_token']).sub;
    let currentUser = await this.userRepository.findOne({
      where: { id: idUser },
    });
    if (currentUser.password === convertToMD5(editPassword.oldPassword)) {
      currentUser.password = convertToMD5(editPassword.newPassword);
      this.userRepository.update(idUser, currentUser);
      res.status(HttpStatus.OK).json({ message: 'Edit Password Successful!' });
    } else {
      throw new InternalServerErrorException('Old Password is not correct');
    }
  }

  async viewProfile(req: any) {
    const idUser = this.jwtService.verify(req.cookies['access_token']).sub;
    const currentUser = await this.userRepository.findOne({
      relations: [
        'followers',
        'followings',
        'posts',
        'posts.products',
        'posts.comments',
        'posts.comments.user',
      ],
      where: { id: idUser },
    });
    return currentUser;
  }

  async updateProfile(userUpdate: UpdateUserDTO, req: any, res: any) {
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    const user = plainToClass(User, userUpdate);
    await this.userRepository.update(idUser, user);
    res.status(HttpStatus.OK).json({ message: 'Update Successfully!' });
  }

  async createUser(createUser: CreateUserDTO) {
    const user = await this.userRepository.save(createUser);
    return user;
  }
}

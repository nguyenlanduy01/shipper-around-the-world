import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Req,
  Res,
  Post,
  UseInterceptors,
  UploadedFile,
  Render,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { User } from 'src/entities/user.entity';
import { UserService } from './user.service';
import { CreateUserDTO } from 'src/dtos/user/create-user.dto';
import { UpdateUserDTO } from 'src/dtos/user/update-user.dto';
import { EditPasswordDTO } from 'src/dtos/user/edit-password.dto';
import { FollowService } from './follow.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { AddressService } from '../address/address.service';
import { CreateAddressDTO } from 'src/dtos/address/create-address.dto';
import { UpdateAddressDTO } from 'src/dtos/address/update-address.dto';
@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly followService: FollowService,
    private readonly addressService: AddressService,
  ) {}

  @Get('all')
  async getAllUser(@Req() req: Request) {
    return this.userService.getAllUser(req);
  }

  @Get('addresses')
  async getAddresses(@Req() req: Request) {
    return this.addressService.getAddresses(req);
  }

  @Get('address/:id')
  async getAddress(@Param('id') id: string) {
    return this.addressService.getAddress(id);
  }

  @Post('address')
  async createAddress(
    @Body() createAddressDTO: CreateAddressDTO,
    @Req() req: Request,
  ) {
    return this.addressService.createAddress(createAddressDTO, req);
  }

  @Patch('address/:id')
  async updateAddress(
    @Param('id') id: string,
    @Body() updateAddressDTO: UpdateAddressDTO,
    @Req() req: Request,
  ) {
    return this.addressService.updateAddress(id, updateAddressDTO, req);
  }

  @Delete('address/:id')
  async deleteAddress(@Param('id') id: string, @Req() req: Request) {
    return this.addressService.deleteAddress(id, req);
  }

  // Lấy ra danh sách người mình đang follow
  @Get('following')
  async getFollowing(@Req() req: Request) {
    return this.followService.getAllFollowing(req);
  }

  // Lấy ra danh sách người đang follow mình
  @Get('follower')
  async getFollower(@Req() req: Request) {
    return this.followService.getAllFollower(req);
  }

  @Render('view-profile')
  @Get('profile/:id')
  async viewProfileUser(@Param('id') id: string) {
    return { id: id };
  }

  // Lấy ra danh sách following của 1 người
  @Get('following/:id')
  async getFollowingByID(@Param('id') id: string) {
    return this.followService.getAllFollowingByID(id);
  }

  // Lấy ra danh sách người đang follower mình
  @Get('follower/:id')
  async getFollowerByID(@Param('id') id: string) {
    return this.followService.getAllFollowerByID(id);
  }

  // Follow 1 user khác
  @Post('follow/:id')
  async follow(
    @Res({ passthrough: true }) res: Response,
    @Req() req: Request,
    @Param('id') id: string,
  ) {
    return this.followService.followUser(res, req, id);
  }

  // Unfollow 1 user khác
  @Delete('unfollow/:id')
  async unFollow(
    @Res({ passthrough: true }) res: Response,
    @Req() req: Request,
    @Param('id') id: string,
  ) {
    return this.followService.unfollowUser(res, req, id);
  }

  // Xem profile của bản thân
  @Get('viewprofile')
  async viewProfile(@Req() req: Request) {
    return this.userService.viewProfile(req);
  }

  // Sửa password của bản thân
  @Patch('editpassword')
  async editPassword(
    @Body() editPassword: EditPasswordDTO,
    @Req() req: Request,
    @Res({ passthrough: true }) res: Response,
  ) {
    return this.userService.editPassword(editPassword, req, res);
  }

  // Chỉnh sửa profile của bản thân
  @Patch('updateprofile')
  async updateProfile(
    @Res({ passthrough: true }) res: Response,
    @Body() updateUser: UpdateUserDTO,
    @Req() req: Request,
  ) {
    return this.userService.updateProfile(updateUser, req, res);
  }

  // Xem trang cá nhân của người khác
  @Get(':id')
  async getUser(@Param('id') id: number): Promise<User> {
    return this.userService.getUserById(id);
  }

  @Post()
  async createUser(@Body() userDTO: CreateUserDTO) {
    return this.userService.createUser(userDTO);
  }

  @Patch(':id')
  async updateUser(@Param('id') id: number, @Body() updateUser: UpdateUserDTO) {
    return this.userService.updateUser(id, updateUser);
  }

  @Delete(':id')
  async deleteUser(@Param('id') id: number) {
    return this.userService.deleteUser(id);
  }
}

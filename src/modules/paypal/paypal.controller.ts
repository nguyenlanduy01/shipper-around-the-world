import { Controller, Post, Body, Get, Req, Res, Render } from '@nestjs/common';
import { Request, Response } from 'express';
import { Public } from 'src/utils/util';
import { PaypalService } from './paypal.service';

@Controller('payments')
export class PaypalController {
  constructor(private readonly paypalService: PaypalService) {}

  @Render('payment-histories')
  @Get('histories')
  async getHistories() {}

  @Render('transaction')
  @Get('transaction')
  async getTransaction() {}

  @Get('allhistory')
  async getAllHistory(@Req() req: Request) {
    return this.paypalService.getAllHistory(req);
  }

  @Public()
  @Post('deposit')
  async createPayment(
    @Body() body: { amount: number; description: string },
    @Req() req: Request,
  ) {
    return this.paypalService.createPayments(body, req);
  }

  @Public()
  @Get('success')
  async executePayment(@Req() req: Request, @Res() res: Response) {
    return this.paypalService.executePayments(req, res);
  }

  @Public()
  @Get('cancel')
  async cancel(@Req() req: Request, @Res() res: Response) {
    return this.paypalService.cancelPayments(req, res);
  }

  @Post('withdraw')
  async transferMoney(
    @Body() body: { amount: number; receiptEmail: string },
    @Req() req: Request,
    @Res() res: Response,
  ) {
    return this.paypalService.transfer(body, req, res);
  }
}

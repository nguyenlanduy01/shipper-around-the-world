import { Module } from '@nestjs/common';
import { PaypalService } from './paypal.service';
import { PaypalController } from './paypal.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TransactionRepository } from 'src/repositories/transaction.repository';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/auth/constants';
import { UserRepository } from 'src/repositories/user.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([TransactionRepository, UserRepository]),
    JwtModule.register({
      secret: jwtConstants.secret,
    }),
  ],
  controllers: [PaypalController],
  providers: [PaypalService],
})
export class PaypalModule {}

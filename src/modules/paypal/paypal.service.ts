import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as paypal from 'paypal-rest-sdk';
import { Transaction } from 'src/entities/transaction.entity';
import { StatusTransaction } from 'src/enums/status-transaction.enum';
import { TypeTransaction } from 'src/enums/type-transaction.enum';
import { TransactionRepository } from 'src/repositories/transaction.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { Cron } from '@nestjs/schedule';

@Injectable()
export class PaypalService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly transactionRepository: TransactionRepository,
    private readonly jwtService: JwtService,
  ) {
    paypal.configure({
      mode: 'sandbox',
      client_id: process.env.PAYPAL_CLIENT_ID,
      client_secret: process.env.PAYPAL_CLIENT_SECRET,
    });
  }

  TIMEOUT_DURATION = 3 * 60 * 60 * 1000;

  async updatePaymentStatus(paymentId: string) {
    const payments = await this.transactionRepository.find({
      payment_id: paymentId,
    });
    if (
      payments &&
      payments.length === 1 &&
      payments[0].status === StatusTransaction.PROCESSING
    ) {
      payments[0].link = null;
      const payment = new Transaction();
      payment.payment_id = payments[0].payment_id;
      payment.coin = payments[0].coin;
      payment.receiver_id = payments[0].receiver_id;
      payment.sender_id = payments[0].sender_id;
      payment.type = payments[0].type;
      payment.description = payments[0].description;
      payment.timestamp = new Date();
      payment.type = payments[0].type;
      payment.status = StatusTransaction.FAIL;
      await this.transactionRepository.save(payment);
      await this.transactionRepository.update(payments[0].id, payments[0]);
    }
  }

  @Cron('* 45 2 * * *')
  async checkPendingPayments() {
    const pendingPayments = await this.transactionRepository.find({
      where: {
        status: StatusTransaction.PROCESSING,
        type: TypeTransaction.DEPOSIT,
      },
    });

    for (const payment of pendingPayments) {
      const elapsedTime = Date.now() - payment.timestamp.getTime();
      if (elapsedTime >= this.TIMEOUT_DURATION) {
        this.updatePaymentStatus(payment.payment_id);
      } else {
        setTimeout(() => {
          this.updatePaymentStatus(payment.payment_id);
        }, this.TIMEOUT_DURATION - elapsedTime);
      }
    }
  }

  async createPayments(body: any, req: any) {
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    const user = await this.userRepository.findOne({
      where: { id: idUser },
    });
    const paymentAction = await this.transactionRepository.find({
      where: { sender_id: idUser, type: TypeTransaction.DEPOSIT },
      order: { id: 'DESC' },
      take: 1,
    });
    if (
      (paymentAction.length === 1 &&
        paymentAction[0].status !== StatusTransaction.PROCESSING) ||
      paymentAction.length === 0
    ) {
      const amount = (Number(body.amount) * 4.0) / 100 + Number(body.amount);
      const description: string = 'CK ' + user.username;
      const payment = {
        intent: 'sale',
        payer: {
          payment_method: 'paypal',
        },
        redirect_urls: {
          return_url: 'http://localhost:3000/payments/success',
          cancel_url: 'http://localhost:3000/payments/cancel',
        },
        transactions: [
          {
            item_list: {
              items: [
                {
                  name: 'Deposit',
                  sku: 'deposit',
                  price: amount,
                  currency: 'USD',
                  quantity: 1,
                },
              ],
            },
            amount: {
              currency: 'USD',
              total: amount.toString(),
            },
            description: description,
          },
        ],
      };

      return new Promise((resolve, reject) => {
        paypal.payment.create(payment, async (error, payment) => {
          if (error) {
            reject(
              new HttpException(
                'Payment creation failed',
                HttpStatus.INTERNAL_SERVER_ERROR,
              ),
            );
          } else {
            const transaction = new Transaction();
            transaction.timestamp = new Date(payment.create_time);
            transaction.description = payment.transactions[0].description;
            transaction.coin =
              (Number(payment.transactions[0].amount.total) / 104) * 100;
            transaction.sender_id = user.id;
            transaction.receiver_id = 1;
            transaction.type = TypeTransaction.DEPOSIT;
            transaction.status = StatusTransaction.PROCESSING;
            transaction.payment_id = payment.id;

            const approvalUrl = payment.links.find(
              (link) => link.rel === 'approval_url',
            ).href;
            transaction.link = approvalUrl;
            await this.transactionRepository.save(transaction);

            resolve({ approvalUrl });
          }
        });
      });
    } else {
      throw new BadRequestException('There are still pending transactions');
    }
  }

  async executePayments(req: any, res: any) {
    const paymentExecution = {
      payer_id: req.query.PayerID,
    };
    paypal.payment.execute(
      req.query.paymentId,
      paymentExecution,
      async (error, payment) => {
        if (error) {
          new HttpException(
            'Payment execution failed',
            HttpStatus.INTERNAL_SERVER_ERROR,
          );
        } else {
          const removeLink = await this.transactionRepository.find({
            where: { payment_id: payment.id },
          });
          if (removeLink.length <= 1) {
            removeLink[0].link = null;
            await this.transactionRepository.update(
              removeLink[0].id,
              removeLink[0],
            );
            const transaction = new Transaction();
            transaction.timestamp = new Date(payment.update_time);
            transaction.description = payment.transactions[0].description;
            transaction.coin =
              (Number(payment.transactions[0].amount.total) / 104) * 100;
            transaction.payment_id = payment.id;
            const username = payment.transactions[0].description.split(' ')[1];
            const user = await this.userRepository.findOne({
              where: { username: username },
            });
            transaction.sender_id = user.id;
            transaction.receiver_id = 1;
            transaction.type = TypeTransaction.DEPOSIT;
            transaction.status = StatusTransaction.DONE;
            user.money +=
              (Number(payment.transactions[0].amount.total) / 104) * 100;
            await this.userRepository.update(user.id, user);
            await this.transactionRepository.save(transaction);
            res.render('transaction', { message: 'Deposit successfully!' });
          }
        }
      },
    );
  }

  async cancelPayments(req: any, res: any) {
    const link = `https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=${req.query.token}`;
    try {
      const payment = await this.transactionRepository.find({
        where: { link: link },
      });
      if (payment.length <= 1) {
        const cancelPayment = new Transaction();
        cancelPayment.coin = payment[0].coin;
        cancelPayment.receiver_id = payment[0].receiver_id;
        cancelPayment.sender_id = payment[0].sender_id;
        cancelPayment.description = payment[0].description;
        cancelPayment.payment_id = payment[0].payment_id;
        cancelPayment.type = TypeTransaction.DEPOSIT;
        cancelPayment.status = StatusTransaction.FAIL;
        cancelPayment.timestamp = new Date();

        payment[0].link = null;
        await this.transactionRepository.save([cancelPayment, payment[0]]);
        res.render('transaction', { message: 'Cancelled deposit' });
      }
    } catch (e) {
      throw new BadRequestException('Cancel fail');
    }
  }

  async transfer(body: any, req: any, res: any) {
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    const user = await this.userRepository.findOne({
      where: { id: idUser },
    });

    const amount = Number(body.amount - (body.amount * 4) / 100);

    if (user.money >= body.amount) {
      const senderBatchId = Math.random().toString(36).substring(9);
      const payout = {
        sender_batch_header: {
          sender_batch_id: senderBatchId,
          email_subject: 'You have a payout!',
        },
        items: [
          {
            recipient_type: 'EMAIL',
            amount: {
              value: amount,
              currency: 'USD',
            },
            receiver: body.receiptEmail,
            note: 'Thank you.',
          },
        ],
      };

      return new Promise((resolve, reject) => {
        paypal.payout.create(payout, async (error, payment) => {
          if (error) {
            reject(
              new HttpException(
                'Payment execution failed',
                HttpStatus.INTERNAL_SERVER_ERROR,
              ),
            );
          } else {
            const transaction = new Transaction();
            transaction.timestamp = new Date();
            transaction.coin = Number(body.amount);
            transaction.payment_id = payment.id;
            transaction.sender_id = 1;
            transaction.receiver_id = user.id;
            transaction.type = TypeTransaction.WITHDRAW;
            transaction.status = StatusTransaction.DONE;
            transaction.payment_id = senderBatchId;
            transaction.description = 'Withdraw';
            user.money -= Number(body.amount);
            await this.userRepository.update(user.id, user);
            await this.transactionRepository.save(transaction);
            res.render('transaction', { message: 'Withdraw Successfully!' });
          }
        });
      });
    } else {
      throw new BadRequestException('Money is not enough');
    }
  }

  async getAllHistory(req: any) {
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    const transactions = await this.transactionRepository.find({
      relations: ['sender', 'receiver'],
      where: [{ sender_id: idUser }, { receiver_id: idUser }],
      order: { created_at: 'DESC' },
    });
    if (transactions) {
      return transactions;
    } else {
      return [];
    }
  }
}

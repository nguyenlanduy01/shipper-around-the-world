import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { jwtConstants } from 'src/auth/constants';
import { JwtModule } from '@nestjs/jwt';
import { CommentRepository } from 'src/repositories/comment.repository';
import { CommentService } from './comment.service';
import { PostRepository } from 'src/repositories/post.repository';
@Module({
  imports: [
    TypeOrmModule.forFeature([CommentRepository, PostRepository]),
    JwtModule.register({
      secret: jwtConstants.secret,
    }),
  ],
  controllers: [],
  providers: [CommentService],
})
export class CommentModule {}

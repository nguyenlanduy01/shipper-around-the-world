import {
  ForbiddenException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CreateCommentDTO } from 'src/dtos/comment/create-comment.dto';
import { CommentRepository } from 'src/repositories/comment.repository';
import { Comment } from 'src/entities/comment.entity';
import { plainToClass } from 'class-transformer';
import { UpdateCommentDTO } from 'src/dtos/comment/update-comment.dto';
import { PostRepository } from 'src/repositories/post.repository';

@Injectable()
export class CommentService {
  constructor(
    private readonly postRepository: PostRepository,
    private readonly commentRepository: CommentRepository,
    private readonly jwtService: JwtService,
  ) {}

  async userComment(id: string, comment: CreateCommentDTO, req: any) {
    const idNum: number = Number(id);
    const user_id: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );

    const post = await this.postRepository.findOne(idNum);
    if (post) {
      const commentUser = plainToClass(Comment, comment);
      commentUser.user_id = user_id;
      commentUser.post_id = idNum;
      return await this.commentRepository.save(commentUser);
    } else {
      throw new InternalServerErrorException('Post does not exist');
    }
  }

  async userEditComment(
    postid: string,
    id: string,
    commentUpdate: UpdateCommentDTO,
    req: any,
  ) {
    const post = await this.postRepository.findOne(postid);
    if (post) {
      const user_id: number = Number(
        this.jwtService.verify(req.cookies['access_token']).sub,
      );
      const idNum: number = Number(id);
      const comment = await this.commentRepository.findOne({
        where: { user_id: user_id, id: idNum, post_id: post.id },
      });
      if (comment) {
        await this.commentRepository.update(idNum, commentUpdate);
      } else {
        throw new InternalServerErrorException('Comment does not exist');
      }
    } else {
      throw new InternalServerErrorException('Post does not exist');
    }
  }

  async userDeleteComment(postid: string, id: string, req: any) {
    const post = await this.postRepository.findOne(postid);
    if (post) {
      const user_id: number = Number(
        this.jwtService.verify(req.cookies['access_token']).sub,
      );
      const idNum: number = Number(id);
      const comment = await this.commentRepository.findOne({
        where: { user_id: user_id, id: idNum, post_id: post.id },
      });
      if (comment) {
        await this.commentRepository.delete(idNum);
      } else {
        throw new InternalServerErrorException('Comment does not exist');
      }
    } else {
      throw new InternalServerErrorException('Post does not exist');
    }
  }
}

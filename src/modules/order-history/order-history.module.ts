import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { jwtConstants } from 'src/auth/constants';
import { OrderRepository } from 'src/repositories/order.repository';
import { OrderHistoriesRepository } from 'src/repositories/order-histories.repository';
import { OrderHistoryService } from './order-history.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([OrderRepository, OrderHistoriesRepository]),
    JwtModule.register({
      secret: jwtConstants.secret,
    }),
  ],
  providers: [OrderHistoryService],
})
export class OrderHistoryModule {}

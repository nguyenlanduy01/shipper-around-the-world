import {
  Injectable,
  InternalServerErrorException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { plainToClass } from 'class-transformer';
import { CreateOrderHistoryDTO } from 'src/dtos/order-history/create-order-history.dto';
import { OrderHistories } from 'src/entities/order-histories.entity';
import { OrderHistoryStatus } from 'src/enums/order-history-status.enum';
import { OrderStatus } from 'src/enums/order-status.enum';
import { OrderHistoriesRepository } from 'src/repositories/order-histories.repository';
import { OrderRepository } from 'src/repositories/order.repository';

@Injectable()
export class OrderHistoryService {
  constructor(
    private readonly orderRepository: OrderRepository,
    private readonly jwtService: JwtService,
    private readonly orderHistoryRepository: OrderHistoriesRepository,
  ) {}

  async getAllOrderHistories() {
    return await this.orderHistoryRepository.find();
  }

  async getOrderHistoriesById(id: string) {
    const orderId: number = Number(id);
    return await this.orderHistoryRepository.find({
      where: { order_id: orderId },
    });
  }

  async createOrderHistory(
    createOrderHistory: CreateOrderHistoryDTO,
    id: string,
    req: any,
  ) {
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    const orderId: number = Number(id);
    const order = await this.orderRepository.findOne({
      where: { id: orderId },
    });
    if (order) {
      if (order.shipper_id === idUser) {
        if (
          order.status === OrderStatus.CONFIRMED ||
          order.status === OrderStatus.PROCESSING
        ) {
          const orderHistoryInput = plainToClass(
            OrderHistories,
            createOrderHistory,
          );
          orderHistoryInput.order_id = orderId;
          if (orderHistoryInput.status === OrderHistoryStatus.DONE) {
            order.status = OrderStatus.DONE;
            await this.orderRepository.update(order.id, order);
          }
          if (order.status === OrderStatus.CONFIRMED) {
            order.status = OrderStatus.PROCESSING;
            await this.orderRepository.update(order.id, order);
          }
          return await this.orderHistoryRepository.save(orderHistoryInput);
        } else {
          throw new InternalServerErrorException('Order does not confirm');
        }
      } else {
        throw new UnauthorizedException();
      }
    } else {
      throw new InternalServerErrorException('Order does not exist');
    }
  }
}

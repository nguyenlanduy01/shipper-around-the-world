import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Render,
  Req,
} from '@nestjs/common';
import { PostService } from './post.service';
import { Request } from 'express';
import { CreatePostDTO } from 'src/dtos/post/create-post.dto';
import { UpdatePostDTO } from 'src/dtos/post/update-post.dto';
import { CreateCommentDTO } from 'src/dtos/comment/create-comment.dto';
import { CommentService } from '../comment/comment.service';
import { UpdateCommentDTO } from 'src/dtos/comment/update-comment.dto';

@Controller('post')
export class PostController {
  constructor(
    private readonly postService: PostService,
    private readonly commentService: CommentService,
  ) {}

  @Get('/getPostsShow')
  async getPostsShow(@Req() req: Request) {
    return this.postService.getPostsShow(req);
  }

  @Post('/:id/comment')
  async userComment(
    @Param('id') id: string,
    @Body() comment: CreateCommentDTO,
    @Req() req: Request,
  ) {
    return this.commentService.userComment(id, comment, req);
  }

  @Patch('/:postid/comment/:id')
  async userEditComment(
    @Param('id') id: string,
    @Param('postid') postid: string,
    @Body() commentUpdate: UpdateCommentDTO,
    @Req() req: Request,
  ) {
    return this.commentService.userEditComment(postid, id, commentUpdate, req);
  }

  @Delete('/:postid/comment/:id')
  async userDeleteComment(
    @Param('postid') postid: string,
    @Param('id') id: string,
    @Req() req: Request,
  ) {
    return this.commentService.userDeleteComment(postid, id, req);
  }

  @Get(':id/getlistusercomment')
  async getListUserComment(@Param('id') id: string) {
    return this.postService.getListUserComment(id);
  }

  // Xem tất cả các post của mình
  @Get('/myposts')
  async getAllPostOfUser(@Req() req: Request) {
    return this.postService.getAllPostOfUser(req);
  }

  // Xem chi tiết post
  @Get(':id')
  async getPost(@Param('id') id: string) {
    return this.postService.getPost(id);
  }

  @Patch(':id/edit')
  async userEditPost(
    @Param('id') id: string,
    @Req() req: Request,
    @Body() updatePost: UpdatePostDTO,
  ) {
    return this.postService.userEditPost(id, req, updatePost);
  }

  @Delete(':id/delete')
  async userDeletePost(@Param('id') id: string, @Req() req: Request) {
    return this.postService.userDeletePost(id, req);
  }

  // Up bài post bằng nick của mình
  @Post('/up')
  async upPost(@Body() createPost: CreatePostDTO, @Req() req: Request) {
    return this.postService.upPost(createPost, req);
  }

  // Lấy ra tất cả các post
  @Render('home')
  @Get()
  async getPosts() {
    return this.postService.getPosts();
  }

  @Post()
  async createPost(@Body() createPost: CreatePostDTO) {
    return this.postService.createPost(createPost);
  }

  // Update post bằng post_id (Tất cả mọi người đều có thể upload)
  @Patch(':id')
  async updatePost(@Param('id') id: string, @Body() updatePost: UpdatePostDTO) {
    return this.postService.updatePost(id, updatePost);
  }

  // Delete post bằng post_id (Tất cả mọi người đều có thể delete)
  @Delete(':id')
  async deletePost(@Param('id') id: string) {
    return this.postService.deletePost(id);
  }
}

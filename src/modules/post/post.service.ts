import {
  BadRequestException,
  ForbiddenException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { plainToClass } from 'class-transformer';
import { CreatePostDTO } from 'src/dtos/post/create-post.dto';
import { UpdatePostDTO } from 'src/dtos/post/update-post.dto';
import { Post } from 'src/entities/post.entity';
import { Product } from 'src/entities/product.entity';
import { CommentRepository } from 'src/repositories/comment.repository';
import { OrderRepository } from 'src/repositories/order.repository';
import { PostRepository } from 'src/repositories/post.repository';
import { ProductRepository } from 'src/repositories/product.repository';
import { UserRepository } from 'src/repositories/user.repository';

@Injectable()
export class PostService {
  constructor(
    private readonly postRepository: PostRepository,
    private readonly productRepository: ProductRepository,
    private readonly userRepository: UserRepository,
    private readonly jwtService: JwtService,
    private readonly orderRepository: OrderRepository,
    private readonly commentRepository: CommentRepository,
  ) {}

  async getAllPostOfUser(req: any) {
    const idUser = this.jwtService.verify(req.cookies['access_token']).sub;
    return this.postRepository.find({
      relations: ['products', 'comments', 'comments.user'],
      where: { user_id: idUser },
    });
  }

  async getPostsShow(req: any) {
    const idUser = this.jwtService.verify(req.cookies['access_token']).sub;
    const posts = await this.postRepository.find({
      relations: ['products', 'comments', 'comments.user', 'user'],
      order: { created_at: 'DESC' },
      where: { isHide: 0, user_id: idUser },
    });

    if (!posts) {
      return [];
    }

    return posts;
  }

  async getPosts() {
    const posts = await this.postRepository.find({
      relations: ['products', 'comments', 'comments.user', 'user'],
      order: { created_at: 'DESC' },
      where: { isHide: 0 },
    });

    return posts;
  }

  async getPost(id: string) {
    const idNum: number = Number(id);
    return await this.postRepository.find({
      relations: ['products'],
      where: { id: idNum },
    });
  }

  async upPost(createPost: CreatePostDTO, req: any) {
    const idUser = this.jwtService.verify(req.cookies['access_token']).sub;
    const postInput = plainToClass(Post, createPost);
    postInput.user_id = idUser;
    const post = await this.postRepository.save(postInput);
    for (const productDTO of createPost.products) {
      const product = plainToClass(Product, productDTO);
      product.post = post;
      await this.productRepository.save(product);
    }
    return post;
  }

  async createPost(createPost: CreatePostDTO) {
    const postInput = plainToClass(Post, createPost);
    const post = await this.postRepository.save(postInput);
    for (const productDTO of createPost.products) {
      const product = plainToClass(Product, productDTO);
      product.post = post;
      await this.productRepository.save(product);
    }
    return post;
  }

  async updatePost(id: string, updatePost: UpdatePostDTO) {
    const idNum: number = Number(id);
    const { user_id, content, products } = updatePost;
    const post = await this.postRepository.findOne({
      where: { id: idNum },
      relations: ['products'],
    });

    if (!post) {
      throw new NotFoundException(`Post with id ${id} not found`);
    }

    const postUpdate = new Post();

    if (content) {
      postUpdate.content = content;
    }

    if (user_id) {
      postUpdate.user_id = user_id;
    }

    const updatedPost = await this.postRepository.update(idNum, postUpdate);

    if (products) {
      for (const productDto of products) {
        const product = post.products.find((p) => p.id == productDto.id);

        if (product) {
          await this.productRepository.update(product.id, productDto);
        }
      }
    }
    return await this.postRepository.findOne({
      where: { id: idNum },
      relations: ['products'],
    });
  }

  async deletePost(id: string) {
    const idNum: number = Number(id);
    const post = await this.postRepository.findOne(idNum);
    const order = await this.orderRepository.findOne({
      where: { post_id: post.id },
    });
    if (!order) {
      if (!post) {
        throw new NotFoundException(`Post with id ${id} not found`);
      }

      if (post.products) {
        for (const product of post.products) {
          await this.productRepository.softDelete(product.id);
        }
      }

      if (post.comments) {
        for (const comment of post.comments) {
          await this.commentRepository.softDelete(comment.id);
        }
      }

      await this.postRepository.softDelete(idNum);
    } else {
      throw new BadRequestException("Can't delete created order");
    }
  }

  async userEditPost(id: string, req: any, updatePost: UpdatePostDTO) {
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );

    const idNum: number = Number(id);
    const { content, products } = updatePost;
    const post = await this.postRepository.findOne({
      where: { id: idNum, user_id: idUser },
      relations: ['products'],
    });

    if (!post) {
      throw new ForbiddenException();
    }

    const postUpdate = new Post();

    if (content) {
      postUpdate.content = content;
    }

    const updatedPost = await this.postRepository.update(idNum, postUpdate);

    if (products) {
      for (const productDto of products) {
        const product = post.products.find((p) => p.id == productDto.id);

        if (product) {
          await this.productRepository.update(product.id, productDto);
        }
      }
    }
    return await this.postRepository.findOne({
      where: { id: idNum },
      relations: ['products'],
    });
  }

  async userDeletePost(id: string, req: any) {
    const idNum: number = Number(id);
    const idUser: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    const post = await this.postRepository.findOne({
      where: { id: idNum, user_id: idUser },
    });
    if (!post) {
      throw new ForbiddenException();
    }
    if (post.products) {
      for (const product of post.products) {
        await this.productRepository.softDelete(product.id);
      }
    }

    if (post.comments) {
      for (const product of post.comments) {
        await this.productRepository.softDelete(product.id);
      }
    }

    await this.postRepository.softDelete(idNum);
  }

  async getListUserComment(id: string) {
    const idPostNum: number = Number(id);
    const post = await this.postRepository.findOne({
      relations: ['comments', 'comments.user'],
      where: { id: idPostNum },
    });

    if (!post) {
      throw new InternalServerErrorException('Post does not exist');
    }

    if (post.comments.length !== 0) {
      const users = post.comments.map((value) => {
        return value.user_id;
      });
      const listUserComment = users.filter((item, index) => {
        return users.indexOf(item) === index;
      });
      return await this.userRepository
        .createQueryBuilder()
        .where('id IN (:...ids)', { ids: listUserComment })
        .andWhere('role = :role', { role: 2 })
        .getMany();
    } else {
      return [];
    }
  }
}

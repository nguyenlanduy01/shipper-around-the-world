import { Module } from '@nestjs/common';
import { PostController } from './post.controller';
import { PostService } from './post.service';
import { PostRepository } from 'src/repositories/post.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { jwtConstants } from 'src/auth/constants';
import { JwtModule } from '@nestjs/jwt';
import { ProductRepository } from 'src/repositories/product.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { CommentRepository } from 'src/repositories/comment.repository';
import { CommentService } from '../comment/comment.service';
import { OrderRepository } from 'src/repositories/order.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      PostRepository,
      ProductRepository,
      UserRepository,
      CommentRepository,
      OrderRepository,
    ]),
    JwtModule.register({
      secret: jwtConstants.secret,
    }),
  ],
  controllers: [PostController],
  providers: [PostService, CommentService],
})
export class PostModule {}

import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { OrderProduct } from 'src/entities/order-product.entity';
import { OrderProductRepository } from 'src/repositories/order-product.repository';
import { OrderRepository } from 'src/repositories/order.repository';

@Injectable()
export class OrderProductService {
  constructor(
    private readonly orderRepository: OrderRepository,
    private readonly jwtService: JwtService,
    private readonly orderProductRepository: OrderProductRepository,
  ) {}

  async createOrderProduct(
    product_id: number,
    order_id: number,
    price: number,
    quantity: number,
  ) {
    const orderProduct = new OrderProduct();
    orderProduct.product_id = product_id;
    orderProduct.order_id = order_id;
    orderProduct.price = price;
    orderProduct.quantity = quantity;
    return await this.orderProductRepository.save(orderProduct);
  }
}

import { Module } from '@nestjs/common';
import { OrderProductService } from './order-product.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductRepository } from 'src/repositories/product.repository';
import { OrderRepository } from 'src/repositories/order.repository';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/auth/constants';
import { OrderProductRepository } from 'src/repositories/order-product.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ProductRepository,
      OrderRepository,
      OrderProductRepository,
    ]),
    JwtModule.register({
      secret: jwtConstants.secret,
    }),
  ],
  providers: [OrderProductService],
})
export class OrderProductModule {}

import {
  Controller,
  Get,
  Post,
  UseGuards,
  Req,
  Res,
  Body,
  Query,
  Patch,
  HttpStatus,
  Render,
  InternalServerErrorException,
  ConflictException,
  UnauthorizedException,
  UseFilters,
} from '@nestjs/common';
import { Response, Request } from 'express';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
import { LocalAuthGuard } from './auth/guards/local-auth.guard';
import { CreateUserDTO } from './dtos/user/create-user.dto';
import { Public } from './utils/util';
import { EditPasswordDTO } from './dtos/user/edit-password.dto';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly authService: AuthService,
  ) {}

  @UseGuards(LocalAuthGuard)
  @Public()
  @Post('auth/login')
  async login(@Req() req: Request, @Res() res: Response) {
    return await this.authService.login(req.user, res);
  }

  @Public()
  @Post('auth/signup')
  signUp(@Res() res: Response, @Body() userDTO: CreateUserDTO) {
    return this.authService.signUp(res, userDTO);
  }

  @Public()
  @Get('auth/confirm')
  async confirm(@Res() res: Response, @Query('token') token: string) {
    return this.authService.confirm(res, token);
  }

  @Public()
  @Get('auth/logout')
  async logout(@Res() res: Response) {
    return this.authService.logout(res);
  }

  @Render('search')
  @Get('search')
  async getSearch() {}

  @Public()
  @Get('auth/verifyemail')
  async verifyEmail(
    @Res({ passthrough: true }) res: Response,
    @Body('email') email: string,
  ) {
    return this.authService.verifyEmail(res, email);
  }

  @Public()
  @Get('auth/forgetpassword')
  async forgetPassword(
    @Res({ passthrough: true }) res: Response,
    @Body('email') email: string,
  ) {
    return this.authService.forgetPassword(res, email);
  }

  @Public()
  @Patch('auth/changepassword')
  async changePassword(
    @Res({ passthrough: true }) res: Response,
    @Query('token') token: string,
    @Body() editPassword: EditPasswordDTO,
  ) {
    return this.authService.changePassword(res, token, editPassword);
  }

  @Get('/profile')
  async getProfile(@Req() req: Request) {
    return await this.authService.getProfile(req);
  }

  @Render('chatgpt')
  @Get('chatgpt')
  async chatGPT() {}

  @Public()
  @Get()
  @Render('index')
  getHello() {}
}

import { createHash } from 'crypto';
import { SetMetadata } from '@nestjs/common';

export const IS_PUBLIC_KEY = 'isPublic';
export const Public = () => SetMetadata(IS_PUBLIC_KEY, true);

export const convertToMD5 = (password: string): string => {
  return createHash('md5').update(password).digest('hex');
};

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity({
  name: 'addresses',
})
export class Address {
  @PrimaryGeneratedColumn('increment', {
    name: 'id',
    type: 'integer',
  })
  id: number;

  @Column({
    name: 'user_id',
    type: 'integer',
  })
  user_id: number;

  @Column({
    name: 'city',
    type: 'varchar',
    length: 255,
  })
  city: string;

  @Column({
    name: 'district',
    type: 'varchar',
    length: 255,
  })
  district: string;

  @Column({
    name: 'commune',
    type: 'varchar',
    length: 255,
  })
  commune: string;

  @Column({
    name: 'description',
    type: 'text',
    nullable: true,
  })
  description?: string;

  @CreateDateColumn({
    name: 'created_at',
  })
  created_at: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: true,
  })
  updated_at?: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
    nullable: true,
  })
  deleted_at?: Date;

  @ManyToOne((type) => User, (user) => user.id)
  @JoinColumn({
    name: 'user_id',
  })
  user: User;
}

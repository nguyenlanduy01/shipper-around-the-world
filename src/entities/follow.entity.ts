import {
  Entity,
  PrimaryColumn,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity({
  name: 'follows',
})
export class Follow {
  @PrimaryColumn({
    name: 'follower_id',
    type: 'integer',
  })
  follower_id: number;

  @PrimaryColumn({
    name: 'following_id',
    type: 'integer',
  })
  following_id: number;

  @CreateDateColumn({
    name: 'created_at',
  })
  created_at: Date;

  @ManyToOne((type) => User, (user) => user.id)
  @JoinColumn({
    name: 'follower_id',
  })
  follower: User;

  @ManyToOne((type) => User, (user) => user.id)
  @JoinColumn({
    name: 'following_id',
  })
  following: User;
}

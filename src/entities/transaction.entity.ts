import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity({
  name: 'transactions',
})
export class Transaction {
  @PrimaryGeneratedColumn('increment', {
    name: 'id',
    type: 'integer',
  })
  id: number;

  @Column({
    name: 'sender_id',
    type: 'integer',
  })
  sender_id: number;

  @Column({
    name: 'receiver_id',
    type: 'integer',
  })
  receiver_id: number;

  @Column({
    name: 'coin',
    type: 'float',
  })
  coin: number;

  @Column({
    name: 'status',
    type: 'varchar',
    length: 255,
  })
  status: string;

  @Column({
    name: 'type',
    type: 'varchar',
    length: 255,
  })
  type: string;

  @Column({
    name: 'description',
    type: 'text',
    nullable: true,
  })
  description?: string;

  @Column({
    name: 'link',
    type: 'text',
    nullable: true,
  })
  link?: string;

  @Column({
    name: 'timestamp',
    type: 'datetime',
    default: Date.now(),
  })
  timestamp: Date;

  @Column({
    name: 'payment_id',
    type: 'varchar',
    nullable: true,
  })
  payment_id?: string;

  @CreateDateColumn({
    name: 'created_at',
  })
  created_at: Date;

  @ManyToOne((type) => User, (user) => user.id)
  @JoinColumn({
    name: 'sender_id',
  })
  sender: User;

  @ManyToOne((type) => User, (user) => user.id)
  @JoinColumn({
    name: 'receiver_id',
  })
  receiver: User;
}

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
  OneToMany,
} from 'typeorm';
import { Follow } from './follow.entity';
import { Post } from './post.entity';
import { Order } from './order.entity';
import { Address } from './address.entity';

@Entity({
  name: 'users',
})
export class User {
  @PrimaryGeneratedColumn('increment', {
    name: 'id',
    type: 'integer',
  })
  id: number;

  @Column({
    name: 'username',
    type: 'varchar',
    length: 120,
  })
  username: string;

  @Column({
    name: 'password',
    type: 'varchar',
    length: 255,
  })
  password: string;

  @Column({
    name: 'email',
    type: 'varchar',
    unique: true,
    length: 255,
  })
  email: string;

  @Column({
    name: 'name',
    type: 'varchar',
    nullable: true,
    length: 120,
    default: '"No name"',
  })
  name?: string;

  @Column({
    name: 'phone',
    type: 'varchar',
    length: 11,
  })
  phone: string;

  @Column({
    name: 'gender',
    type: 'boolean',
    default: true,
  })
  gender: boolean;

  @Column({
    name: 'birth',
    type: 'date',
    nullable: true,
  })
  birth?: Date;

  @Column({
    name: 'avatar',
    type: 'varchar',
    length: 2048,
    default:
      "'https://res.cloudinary.com/dwpqh1z7n/image/upload/v1683273938/user_avatars/c6e56503cfdd87da299f72dc416023d4_mstxim.jpg'",
  })
  avatar: string;

  @Column({
    name: 'is_active',
    type: 'boolean',
    default: false,
  })
  is_active: boolean;

  @Column({
    name: 'role',
    type: 'integer',
  })
  role: number;

  @Column({
    name: 'money',
    type: 'float',
    default: 0,
  })
  money: number;

  @Column({
    name: 'money_lock',
    type: 'float',
    default: 0,
  })
  money_lock: number;

  @CreateDateColumn({
    name: 'created_at',
  })
  created_at: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: true,
  })
  updated_at?: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
    nullable: true,
  })
  deleted_at?: Date;

  @OneToMany(() => Follow, (follow) => follow.follower)
  followers: Follow[];

  @OneToMany(() => Follow, (follow) => follow.following)
  followings: Follow[];

  @OneToMany(() => Post, (post) => post.user)
  posts: Post[];

  @OneToMany(() => Order, (order) => order.user)
  user_orders: Order[];

  @OneToMany(() => Order, (order) => order.shipper)
  shipper_orders: Order[];

  @OneToMany(() => Address, (address) => address.user)
  addresses: Address[];
}

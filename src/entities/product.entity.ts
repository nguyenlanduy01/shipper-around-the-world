import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Post } from './post.entity';
import { Order } from './order.entity';

@Entity({
  name: 'products',
})
export class Product {
  @PrimaryGeneratedColumn('increment', {
    name: 'id',
    type: 'integer',
  })
  id: number;

  @Column({
    name: 'post_id',
    type: 'integer',
  })
  post_id: number;

  @Column({
    name: 'name',
    type: 'varchar',
    length: 255,
  })
  name: string;

  @Column({
    name: 'image',
    type: 'varchar',
    length: 2048,
  })
  image: string;

  @Column({
    name: 'quantity',
    type: 'integer',
  })
  quantity: number;

  @Column({
    name: 'price',
    type: 'float',
  })
  price: number;

  @Column({
    name: 'description',
    type: 'text',
    nullable: true,
  })
  description?: string;

  @Column({
    name: 'category',
    type: 'varchar',
    length: 255,
  })
  category: string;

  @Column({
    name: 'brand',
    type: 'varchar',
    length: 255,
  })
  brand: string;

  @CreateDateColumn({
    name: 'created_at',
  })
  created_at: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: true,
  })
  updated_at?: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
    nullable: true,
  })
  deleted_at?: Date;

  @ManyToOne((type) => Post, (post) => post.id)
  @JoinColumn({
    name: 'post_id',
  })
  post: Post;
}

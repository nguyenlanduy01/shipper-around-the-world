import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { User } from './user.entity';
import { Post } from './post.entity';
import { Address } from './address.entity';
import { OrderProduct } from './order-product.entity';
import { OrderHistories } from './order-histories.entity';

@Entity({
  name: 'orders',
})
export class Order {
  @PrimaryGeneratedColumn('increment', {
    name: 'id',
    type: 'integer',
  })
  id: number;

  @Column({
    name: 'user_id',
    type: 'integer',
  })
  user_id: number;

  @Column({
    name: 'shipper_id',
    type: 'integer',
  })
  shipper_id: number;

  @Column({
    name: 'address_id',
    type: 'integer',
  })
  address_id: number;

  @Column({
    name: 'post_id',
    type: 'integer',
  })
  post_id: number;

  @Column({
    name: 'ship',
    type: 'float',
  })
  ship: number;

  @Column({
    name: 'total_price',
    type: 'float',
  })
  total_price: number;

  @Column({
    name: 'location',
    type: 'text',
  })
  location: string;

  @Column({
    name: 'description',
    type: 'text',
    nullable: true,
  })
  description?: string;

  @Column({
    name: 'status',
    type: 'varchar',
    length: 255,
    default: 'CONFIRMING',
  })
  status: string;

  @Column({
    name: 'rate',
    type: 'integer',
    nullable: true,
  })
  rate?: number;

  @Column({
    name: 'review',
    type: 'text',
    nullable: true,
  })
  review?: string;

  @Column({
    name: 'received_status',
    type: 'boolean',
    default: false,
  })
  received_status: boolean;

  @Column({
    name: 'user_confirm',
    type: 'boolean',
    default: true,
  })
  user_confirm: boolean;

  @Column({
    name: 'shipper_confirm',
    type: 'boolean',
    nullable: true,
  })
  shipper_confirm?: boolean;

  @Column({
    name: 'reason',
    type: 'text',
    nullable: true,
  })
  reason?: string;

  @CreateDateColumn({
    name: 'created_at',
  })
  created_at: Date;

  @ManyToOne((type) => User, (user) => user.id)
  @JoinColumn({
    name: 'user_id',
  })
  user: User;

  @ManyToOne((type) => User, (user) => user.id)
  @JoinColumn({
    name: 'shipper_id',
  })
  shipper: User;

  @ManyToOne((type) => Post, (post) => post.id)
  @JoinColumn({
    name: 'post_id',
  })
  post: Post;

  @ManyToOne((type) => Address, (address) => address.id)
  @JoinColumn({
    name: 'address_id',
  })
  address: Address;

  @OneToMany(() => OrderProduct, (orderProduct) => orderProduct.order)
  orderProducts: OrderProduct[];

  @OneToMany(() => OrderHistories, (orderHistories) => orderHistories.order)
  orderHistories: OrderHistories[];
}

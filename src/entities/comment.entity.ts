import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { User } from './user.entity';
import { Post } from './post.entity';

@Entity({
  name: 'comments',
})
export class Comment {
  @PrimaryGeneratedColumn('increment', {
    name: 'id',
    type: 'integer',
  })
  id: number;

  @Column({
    name: 'user_id',
    type: 'integer',
  })
  user_id: number;

  @Column({
    name: 'post_id',
    type: 'integer',
  })
  post_id: number;

  @Column({
    name: 'content',
    type: 'text',
  })
  content: string;

  @Column({
    name: 'status',
    type: 'varchar',
    length: 255,
    default: 'POSTED',
  })
  status: string;

  @CreateDateColumn({
    name: 'created_at',
  })
  created_at: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: true,
  })
  updated_at?: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
    nullable: true,
  })
  deleted_at?: Date;

  @ManyToOne((type) => User, (user) => user.id)
  @JoinColumn({
    name: 'user_id',
  })
  user: User;

  @ManyToOne((type) => Post, (post) => post.id)
  @JoinColumn({
    name: 'post_id',
  })
  post: Post;
}

import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Order } from './order.entity';

@Entity({
  name: 'order_histories',
})
export class OrderHistories {
  @PrimaryGeneratedColumn('increment', {
    name: 'id',
    type: 'integer',
  })
  id: number;

  @Column({
    name: 'order_id',
    type: 'integer',
  })
  order_id: number;

  @Column({
    name: 'nation',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  nation?: string;

  @Column({
    name: 'city',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  city?: string;

  @Column({
    name: 'district',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  district?: string;

  @Column({
    name: 'commune',
    type: 'varchar',
    length: 255,
    nullable: true,
  })
  commune?: string;

  @Column({
    name: 'description',
    type: 'text',
    nullable: true,
  })
  description?: string;

  @Column({
    name: 'status',
    type: 'varchar',
    length: 255,
  })
  status: string;

  @CreateDateColumn({
    name: 'created_at',
  })
  created_at: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: true,
  })
  updated_at?: Date;

  @ManyToOne((type) => Order, (order) => order.id)
  @JoinColumn({
    name: 'order_id',
  })
  order: Order;
}

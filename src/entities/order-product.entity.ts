import {
  Entity,
  PrimaryColumn,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  Column,
} from 'typeorm';
import { Product } from './product.entity';
import { Order } from './order.entity';

@Entity({
  name: 'order_product',
})
export class OrderProduct {
  @PrimaryColumn({
    name: 'product_id',
    type: 'integer',
  })
  product_id: number;

  @PrimaryColumn({
    name: 'order_id',
    type: 'integer',
  })
  order_id: number;

  @Column({
    name: 'price',
    type: 'float',
  })
  price: number;

  @Column({
    name: 'quantity',
    type: 'integer',
  })
  quantity: number;

  @CreateDateColumn({
    name: 'created_at',
  })
  created_at: Date;

  @ManyToOne((type) => Product, (product) => product.id)
  @JoinColumn({
    name: 'product_id',
  })
  product: Product;

  @ManyToOne((type) => Order, (order) => order.id)
  @JoinColumn({
    name: 'order_id',
  })
  order: Order;
}

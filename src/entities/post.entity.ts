import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { User } from './user.entity';
import { Product } from './product.entity';
import { Comment } from './comment.entity';

@Entity({
  name: 'posts',
})
export class Post {
  @PrimaryGeneratedColumn('increment', {
    name: 'id',
    type: 'integer',
  })
  id: number;

  @Column({
    name: 'user_id',
    type: 'integer',
  })
  user_id: number;

  @Column({
    name: 'content',
    type: 'text',
  })
  content: string;

  @Column({
    name: 'status',
    type: 'varchar',
    length: 255,
    default: 'POSTED',
  })
  status: string;

  @Column({
    name: 'isHide',
    type: 'boolean',
    default: false,
  })
  isHide: boolean;

  @CreateDateColumn({
    name: 'created_at',
  })
  created_at: Date;

  @UpdateDateColumn({
    name: 'updated_at',
    nullable: true,
  })
  updated_at?: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
    nullable: true,
  })
  deleted_at?: Date;

  @ManyToOne((type) => User, (user) => user.id)
  @JoinColumn({
    name: 'user_id',
  })
  user: User;

  @OneToMany(() => Product, (product) => product.post)
  products: Product[];

  @OneToMany(() => Comment, (comment) => comment.post)
  comments: Comment[];
}

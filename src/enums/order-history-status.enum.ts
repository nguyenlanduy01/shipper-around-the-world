export enum OrderHistoryStatus {
  PROCESSING = 'PROCESSING',
  PURCHASED = 'PURCHASED',
  SHIPPING = 'SHIPPING',
  DONE = 'DONE',
}

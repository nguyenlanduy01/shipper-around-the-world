export enum Role {
  User = 1,
  Shipper = 2,
  Admin = 3,
}
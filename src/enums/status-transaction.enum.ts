export enum StatusTransaction {
  DONE = 'DONE',
  PROCESSING = 'PROCESSING',
  FAIL = 'FAIL',
}

export enum PostStatus {
  POSTED = 'POSTED',
  PROCESSING = 'PROCESSING',
  DONE = 'DONE',
}

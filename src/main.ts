import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as cookieParser from 'cookie-parser';
import { ValidationPipe } from '@nestjs/common';
import { RoleGuard } from './auth/guards/roles.guard';
import { NestExpressApplication } from '@nestjs/platform-express';
import { join } from 'path';
import * as moment from 'moment';
import { AuthExceptionFilter } from './filters/auth-exception-filter.filter';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useStaticAssets(join(__dirname, '..', 'public'));
  app.setBaseViewsDir(join(__dirname, '..', 'views'));
  app.setViewEngine('hbs');
  const hbs = require('hbs');
  hbs.registerHelper('formatDate', function (date) {
    return new Date(date).toLocaleString();
  });

  hbs.registerHelper('getVar', function (varName) {
    return varName;
  });

  hbs.registerHelper('getLocalStorage', function (key) {
    return localStorage.getItem(key);
  });

  hbs.registerHelper('get_length', function (obj) {
    return obj.length;
  });

  hbs.registerHelper('eq', (a, b) => a == b);

  app.use(cookieParser());
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalFilters(new AuthExceptionFilter());
  await app.listen(3000);
}
bootstrap();

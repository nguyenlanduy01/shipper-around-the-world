import {
  ConflictException,
  HttpStatus,
  Injectable,
  InternalServerErrorException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { convertToMD5 } from 'src/utils/util';
import { UserRepository } from 'src/repositories/user.repository';
import { CreateUserDTO } from 'src/dtos/user/create-user.dto';
import { MailService } from './mail.service';
import { EditPasswordDTO } from 'src/dtos/user/edit-password.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly userRepository: UserRepository,
    private readonly mailService: MailService,
  ) {}
  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.userRepository.findOne({
      where: { username: username },
    });
    if (user && user.password == convertToMD5(password)) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }

  async getProfile(req: any) {
    const user_id: number = Number(
      this.jwtService.verify(req.cookies['access_token']).sub,
    );
    const user = await this.userRepository.findOne({
      where: { id: user_id },
    });
    return user;
  }

  async login(user: any, res: any) {
    if (user.is_active === true) {
      const payload = { username: user.username, sub: user.id };
      const access_token = this.jwtService.sign(payload);
      res.cookie('access_token', access_token);
      return res.redirect('/post');
    } else {
      throw new InternalServerErrorException('Email not verified');
    }
  }

  async signUp(res: any, userDTO: CreateUserDTO) {
    const userEmail = await this.userRepository.findOne({
      where: { email: userDTO.email },
    });
    const userUsername = await this.userRepository.findOne({
      where: { username: userDTO.username },
    });
    if (userEmail) {
      throw new ConflictException('Email already exists');
    }
    if (userUsername) {
      throw new ConflictException('Username already exists');
    }

    const hashedPassword = convertToMD5(userDTO.password);
    userDTO.password = hashedPassword;

    const user = await this.userRepository.save(userDTO);

    const payload = { username: user.username, sub: user.id };
    const token = this.jwtService.sign(payload, {
      expiresIn: '10m',
      secret: 'confirmEmail',
    });

    await this.userRepository.save(user);

    await this.mailService.sendConfirmationEmail(user.email, token);

    res.render('index', { message: 'Send email Successfully!' });
  }

  async verifyEmail(res: any, email: string) {
    const existingUser = await this.userRepository.findOne({
      where: { email: email },
    });
    if (!existingUser) {
      throw new InternalServerErrorException('Email does not exist');
    }
    const payload = { username: existingUser.username, sub: existingUser.id };
    const token = this.jwtService.sign(payload, {
      expiresIn: '10m',
      secret: 'confirmEmail',
    });
    await this.mailService.sendConfirmationEmail(email, token);
    res.status(HttpStatus.OK).json({ message: 'Send email Successfully!' });
  }

  async confirm(res: any, token: string) {
    let userId: number;
    try {
      userId = this.jwtService.verify(token, {
        secret: 'confirmEmail',
      }).sub;
    } catch (e) {
      throw new UnauthorizedException('Token expirise');
    }
    const user = await this.userRepository.findOne({
      where: { id: userId },
    });
    user.is_active = true;
    await this.userRepository.save(user);

    res.render('index', { message: 'Account confirmed' });
  }

  async logout(res: any) {
    res.clearCookie('access_token');
    res.render('index', { message: 'Log out successfully!' });
  }

  async forgetPassword(res: any, email: string) {
    const user = await this.userRepository.findOne({ email: email });
    if (user && user.is_active === true) {
      const payload = { username: user.username, sub: user.id };
      const token = this.jwtService.sign(payload, {
        expiresIn: '10m',
        secret: 'email',
      });
      this.mailService.sendForgetPassword(email, token);
      res.status(HttpStatus.OK).json({ message: 'Send mail successfully!' });
    } else {
      throw new InternalServerErrorException('Email does not exist');
    }
  }

  async changePassword(res: any, token: string, editPassword: EditPasswordDTO) {
    let userId: number;
    try {
      userId = this.jwtService.verify(token, {
        secret: 'email',
      }).sub;
    } catch (e) {
      throw new UnauthorizedException('Token expirise');
    }

    let user = await this.userRepository.findOne({ where: { id: userId } });
    user.password = convertToMD5(editPassword.newPassword);
    this.userRepository.update(userId, user);
    res
      .status(HttpStatus.OK)
      .json({ message: 'Change Password Successfully!' });
  }

  async getUserRole(id: number) {
    return (await this.userRepository.findOne({ where: { id: id } })).role;
  }
}

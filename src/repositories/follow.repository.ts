import { Follow } from 'src/entities/follow.entity';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(Follow)
export class FollowRepository extends Repository<Follow> {}

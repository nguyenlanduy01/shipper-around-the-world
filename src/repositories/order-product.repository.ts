import { OrderProduct } from 'src/entities/order-product.entity';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(OrderProduct)
export class OrderProductRepository extends Repository<OrderProduct> {}

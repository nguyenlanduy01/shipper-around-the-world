import { OrderHistories } from 'src/entities/order-histories.entity';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(OrderHistories)
export class OrderHistoriesRepository extends Repository<OrderHistories> {}

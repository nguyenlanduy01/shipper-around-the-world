import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
} from '@nestjs/common';
import { Response } from 'express';

@Catch(HttpException)
export class AuthExceptionFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    if (ctx.getRequest().url.includes('/login')) {
      const er = exception.getResponse();
      response.render('index', { error: er['message'] });
    }
    if (ctx.getRequest().url.includes('/signup')) {
      const er = exception.getResponse();
      response.render('index', { error: er['message'] });
    }
    if (exception.message === 'Token invalid') {
      response.render('index', { error: exception.message });
    }
  }
}

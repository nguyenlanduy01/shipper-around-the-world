import {
  IsNotEmpty,
  IsNumberString,
  IsString,
  IsOptional,
} from 'class-validator';

export class CreateOrderDTO {
  @IsNotEmpty()
  @IsNumberString()
  shipper_id: number;

  @IsNotEmpty()
  @IsNumberString()
  post_id: number;

  @IsNotEmpty()
  @IsNumberString()
  address_id: number;

  @IsNotEmpty()
  @IsNumberString()
  ship: number;

  @IsOptional()
  @IsString()
  description?: string;
}

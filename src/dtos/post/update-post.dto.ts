import {
  IsNotEmpty,
  IsNumberString,
  IsString,
  IsOptional,
  IsArray,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { UpdateProductDTO } from '../product/update-product.dto';
export class UpdatePostDTO {
  @IsOptional()
  @IsString()
  content?: string;

  @IsOptional()
  @IsNumberString()
  user_id?: number;

  @IsOptional()
  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => UpdateProductDTO)
  products?: UpdateProductDTO[];
}

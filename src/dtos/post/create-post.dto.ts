import {
  IsNotEmpty,
  IsNumberString,
  IsString,
  IsOptional,
  IsArray,
  ValidateNested,
  ArrayMinSize,
} from 'class-validator';
import { CreateProductDTO } from '../product/create-product.dto';
import { Type } from 'class-transformer';
export class CreatePostDTO {
  @IsNotEmpty()
  @IsString()
  content: string;

  @IsOptional()
  @IsNumberString()
  user_id?: number;

  @IsArray()
  @ValidateNested({ each: true })
  @Type(() => CreateProductDTO)
  @ArrayMinSize(1)
  products: CreateProductDTO[];
}

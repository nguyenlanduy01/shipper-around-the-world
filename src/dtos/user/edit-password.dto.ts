import { IsOptional } from 'class-validator';
import { IsPassword } from '../../decorators/password.decorator';
import { Match } from '../../decorators/match.decorator';

export class EditPasswordDTO {
  @IsPassword()
  @IsOptional()
  oldPassword?: string;

  @IsPassword()
  newPassword: string;

  @IsPassword()
  @Match('newPassword')
  confirmPassword: string;
}

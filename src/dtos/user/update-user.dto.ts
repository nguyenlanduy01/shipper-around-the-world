import {
  IsBooleanString,
  IsDateString,
  IsEmail,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  IsString,
  Length,
} from 'class-validator';
import { IsPassword } from 'src/decorators/password.decorator';

export class UpdateUserDTO {
  @IsString()
  @Length(1, 120)
  @IsOptional()
  name?: string;

  @IsString()
  @Length(1, 120)
  @IsOptional()
  username?: string;

  @IsOptional()
  @IsPassword()
  password?: string;

  @IsOptional()
  @IsEmail()
  email?: string;

  @IsOptional()
  @IsNumberString()
  @Length(10)
  phone?: string;

  @IsOptional()
  @IsBooleanString()
  gender?: boolean;

  @IsNumberString()
  @IsOptional()
  role?: number;

  @IsOptional()
  @IsDateString()
  birth?: string;

  @IsOptional()
  @IsString()
  avatar?: string;
}

import {
  IsBoolean,
  IsDateString,
  IsEmail,
  IsNotEmpty,
  IsNumberString,
  IsString,
  Length,
} from 'class-validator';
import { IsPassword } from '../../decorators/password.decorator';

export class CreateUserDTO {
  @IsString()
  @Length(1, 120)
  @IsNotEmpty()
  username: string;

  @IsPassword()
  password: string;

  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsNumberString()
  @Length(10)
  phone: string;

  @IsNumberString()
  @IsNotEmpty()
  role: number;
}

import {
  IsNotEmpty,
  IsNumberString,
  IsString,
  IsOptional,
  IsEnum,
} from 'class-validator';
import { OrderHistoryStatus } from 'src/enums/order-history-status.enum';

export class CreateOrderHistoryDTO {
  @IsOptional()
  @IsString()
  image?: string;

  @IsNotEmpty()
  @IsString()
  nation: string;

  @IsNotEmpty()
  @IsString()
  city: string;

  @IsNotEmpty()
  @IsString()
  district: string;

  @IsNotEmpty()
  @IsString()
  commune: string;

  @IsOptional()
  @IsString()
  description?: string;

  @IsEnum(OrderHistoryStatus)
  status: string;
}

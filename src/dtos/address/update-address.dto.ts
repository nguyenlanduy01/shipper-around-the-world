import {
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  IsString,
} from 'class-validator';

export class UpdateAddressDTO {
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  city: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  district: string;

  @IsOptional()
  @IsNotEmpty()
  @IsString()
  commune: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty()
  description?: string;
}

import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { User } from './entities/user.entity';
import { UserModule } from './modules/user/user.module';
import { AuthModule } from './auth/auth.module';
import { APP_FILTER, APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { JwtAuthGuard } from './auth/guards/jwt-auth.guard';
import { RoleGuard } from './auth/guards/roles.guard';
import { JwtService } from '@nestjs/jwt';
import { Follow } from './entities/follow.entity';
import { Product } from './entities/product.entity';
import { Post } from './entities/post.entity';
import { PostModule } from './modules/post/post.module';
import { CommentModule } from './modules/comment/comment.module';
import { Comment } from './entities/comment.entity';
import { Order } from './entities/order.entity';
import { OrderModule } from './modules/order/order.module';
import { Address } from './entities/address.entity';
import { OrderProduct } from './entities/order-product.entity';
import { OrderHistories } from './entities/order-histories.entity';
import { Transaction } from './entities/transaction.entity';
import { PaypalModule } from './modules/paypal/paypal.module';
import { ScheduleModule } from '@nestjs/schedule';
import { AuthExceptionFilter } from './filters/auth-exception-filter.filter';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DB_HOST,
      port: Number.parseInt(process.env.DB_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      entities: [
        User,
        Follow,
        Post,
        Product,
        Comment,
        Order,
        Address,
        OrderProduct,
        OrderHistories,
        Transaction,
      ],
    }),
    UserModule,
    AuthModule,
    PostModule,
    OrderModule,
    CommentModule,
    PaypalModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    JwtService,
    { provide: APP_GUARD, useClass: JwtAuthGuard },
    { provide: APP_GUARD, useClass: RoleGuard },
  ],
})
export class AppModule {}
